<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'real-estate');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'tomcat');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7um[-3`L>: IA$}8mPLSDqyQC6izsAyhnlWIsTiWOr0n{-7KebmTn?L6xL~ |(:V');
define('SECURE_AUTH_KEY',  'O3 ftVoPZW!m(0yU=|1g1ghgdx_V=*4leci&A0s_F$$vz/(UIjwl 1TV8^B[xzOI');
define('LOGGED_IN_KEY',    ' :9)LCbC8$6LbcW&K4-o?T2A??_*@c^4_ez8/;|~2XGXr;FbuPnBf)iI}AVn~w%l');
define('NONCE_KEY',        '.`qfZLy9/PGnY6!Nk>N3Zp`c{6Rd2OG/6a{>7zuNY&Koc6$[D*)o7PwIlA0(O1K,');
define('AUTH_SALT',        '6A@JybcS6k;ES6&=M:;3?QNAga-ajUK2|NlvpXQ7QNy/[_}q$5rBYXX}1Z[VUIrq');
define('SECURE_AUTH_SALT', '1|E#t^ELxdD@jG4X?(y6d~}kORZ/@-Gxvb.[*SM~m-S:~(j?GZ1}Wwm|q|,tXVPj');
define('LOGGED_IN_SALT',   '%v0)6-tF$-F6xrQ*3Nqna%ROku#^/2*-FY+<qh80#DTW3Z^kz1O^|k3V34*j2;[#');
define('NONCE_SALT',       '_n)HTBk MENV+VZXIXom^f`P7R|P3cE-];wqhsY6 v/{kD#Rim2l/A>hd~O5DCKu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 're_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
